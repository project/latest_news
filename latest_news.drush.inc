<?php
/**
 * @file
 * Drush command file
 */

/**
 * Implements hook_drush_command().
 */
function latest_news_drush_command() {
  $items['latest-news'] = array(
    // Description when someone types drush or drush help
    'description' => 'View latest news',
    'aliases' => array('ln'),
    'arguments' => array(
      'number' => 'Number of Latest News nodes to display',
    ),
  );

  return $items;
}

/**
 * Validation for the latest_news command.
 */
function drush_latest_news_validate($number = FALSE) {
  if(!is_numeric($number) || $number < 0) {
    return drush_set_error('LATEST_NEWS_ERROR', dt('@number is not a valid number. Must be an integer greater than zero', array('@number' => $number)));
  }
}

/**
 * Command function for latest_news command.
 */
function drush_latest_news($number = FALSE) {
  // Get latest news
  $query = new EntityFieldQuery();
  $query
  ->entityCondition('entity_type', 'node')
  ->propertyCondition('type', 'latest_news')
  ->propertyOrderBy('created', 'DESC')
  ->range(0, $number);
  $result = $query->execute();

  $nodes = node_load_multiple(array_keys($result['node']));

  if (count($nodes) == 0) {
    drush_set_error('LATEST_NEWS_ERROR', dt('There is no news'));
    return;
  }

  foreach($nodes as $node) {
    drush_print($node->title);
  }

  drush_log(dt('Finished printing latest news'), 'ok');
}
