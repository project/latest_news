<?php
/**
 * @file
 * latest_news.features.inc
 */

/**
 * Implements hook_views_api().
 */
function latest_news_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function latest_news_node_info() {
  $items = array(
    'latest_news' => array(
      'name' => t('Latest News'),
      'base' => 'node_content',
      'description' => t('Latest news articles'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
